<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Form</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
      @csrf
      <p>First name:</p>
      <input type="text" name="namaDepan" />
      <p>Last name:</p>
      <input type="text" name="namaBelakang" />
      <p>Gender:</p>
      <input type="radio" id="male" name="gender" value="male" />
      <label>Male</label><br />
      <input type="radio" id="male" name="gender" value="male" />
      <label>Female</label><br />
      <input type="radio" id="male" name="gender" value="male" />
      <label>Others</label><br />

      <p>Nationaly:</p>
      <select>
        <option value="actual value 1">Indonesia</option>
        <option value="actual value 2">Singapura</option>
        <option value="actual value 3">Malaysia</option>
      </select>
      <p>Language Spoken:</p>
      <input type="checkbox" />
      <label for="#">Bahasa Indonesia</label><br />
      <input type="checkbox" />
      <label for="#">English</label><br />
      <input type="checkbox" />
      <label for="#">Other</label><br />
      <p>Bio:</p>
      <textarea></textarea>
      <button type="submit">Sign Up</button>
    </form>
  </body>
</html>
