<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
      return view('form');
    }

    public function welcome(){
      return view('welcome');
    }
    
    public function welcome_post(Request $request){
      // dd($request["namaBelakang"]);
      // dd($request->all());
      $namaDepan = $request["namaDepan"];
      $namaBelakang = $request["namaBelakang"];
      // return "$namaDepan $namaBelakang";
      return view('welcome',['namaDepan' => $namaDepan],['namaBelakang' => $namaBelakang]);
    }
}
